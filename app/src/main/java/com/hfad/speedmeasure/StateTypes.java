package com.hfad.speedmeasure;

/**
 * Created by helen on 28/01/18.
 */

public enum StateTypes {STOPPED, WALKING, WALKING_FAST, RUNNING, SPRINT, MOTOR_SPEED, AERIAL_SPEED};


