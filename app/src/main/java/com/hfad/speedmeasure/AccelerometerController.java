/**
 * Created by helen on 26/01/18.
 */
package com.hfad.speedmeasure;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import static android.content.Context.SENSOR_SERVICE;

public class AccelerometerController  {

    private SensorManager sensorManager;
    private Sensor accelerometerSensor;
    private AccelerometerSensorListener accelerometerSensorListener;
    private static AccelerometerController controller = null;
    private SensorListener sensorListener;

    private AccelerometerController(SensorListener sensorListener, Context context)
    {
        this.sensorListener = sensorListener;
        accelerometerSensorListener = new AccelerometerSensorListener(this);
        sensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    }

    public static void init(SensorListener sensorListener, Context context)
    {
        if (controller == null )
            controller = new AccelerometerController(sensorListener, context);

    }

    public static synchronized AccelerometerController getController()
    {
        return controller;
    }

    public void startAccelerometer(boolean typeOfProcess)
    {

        enableCalibration(typeOfProcess);

        if (sensorListener != null)
        {
            sensorListener.startListener();
        }

    }
    public void registerAccelerometer()
    {
        sensorManager.registerListener(accelerometerSensorListener, accelerometerSensor, SensorManager.SENSOR_DELAY_UI);
    }

    public void unregisterAccelerometer()
    {
        sensorManager.unregisterListener(accelerometerSensorListener);
    }

    public float[] getCalibrationValues()
    {
        return accelerometerSensorListener.getCalibrationValues();
    }

    public void enableCalibration(boolean value)
    {
        accelerometerSensorListener.enableCalibration(value);
    }

    public boolean isLowAccuracy(int accuracy)
    {
        if (accuracy == sensorManager.SENSOR_STATUS_ACCURACY_LOW || accuracy ==  sensorManager.SENSOR_STATUS_UNRELIABLE || accuracy ==  sensorManager.SENSOR_STATUS_UNRELIABLE)
            return true;
        else
            return false;
    }

    public void finishCalibration()
    {

        sensorListener.stopListener();
    }

    public static void removeController()
    {
        controller = null;
    }

    public void setupCalibration(float[] initialCalibrationValues)
    {
        accelerometerSensorListener.setupCalibrationValues(initialCalibrationValues);
    }

    public void trackerDevice()
    {
        //start speedActivity when movement is detected
        if (sensorListener != null)
        {
            sensorListener.stopListener();
            sensorListener.onAction();
        }

    }

    //interface listener implemented in CalibrationActivity
    public interface SensorListener
    {
        public void startListener();
        public void stopListener();
        public void onAction();
    }

}
