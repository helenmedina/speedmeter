package com.hfad.speedmeasure;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by helen on 31/01/18.
 */

public abstract class DetectorBasedOnGPS extends AppCompatActivity implements GPSController.LocationDetectorListener {

    protected GPSController gpsController = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public abstract void startListener();

    @Override
    public void stopListener() {

        if (gpsController.isSatelliteDetectorEnabled())
        {
            gpsController.removeSatellitesDetector();
        }

        if (gpsController.isGPSDetectorEnabled())
        {
            gpsController.removeGPSDetector();
        }


    }

    @Override
    public abstract void onSatelliteAction(int numberSatellites);

    @Override
    public abstract void onGPSAction(Metrics metrics);
}
