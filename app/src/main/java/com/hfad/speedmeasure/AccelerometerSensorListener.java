package com.hfad.speedmeasure;

/**
 * Created by helen on 25/01/18.
 */


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;


public class AccelerometerSensorListener implements SensorEventListener{

    private float[] mCalibrationValues;
    private boolean bCalibrationEnabled;
    private AccelerometerController controller;
    private double x,y,z;

    public AccelerometerSensorListener(AccelerometerController controller)
    {
        this.controller = controller;
        bCalibrationEnabled = true;
        x = y = z = 0;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            //setup calibration when high or medium accuracy is reached
            if(bCalibrationEnabled)
            {
                if (!controller.isLowAccuracy(event.accuracy)) {
                    setupCalibrationValues(event.values);
                    enableCalibration(false);
                    controller.finishCalibration();
                }

            }
            else
            {
                //get current values from acellerometer
                x = event.values[0] - mCalibrationValues[0];
                y = event.values[1] - mCalibrationValues[1];
                z = event.values[2] - mCalibrationValues[2];

                //calculate distance between current and calibration values
                double delta = Math.sqrt(x * x + y * y + z*z);

                if(delta > 1.5 ){
                    controller.trackerDevice();
                }

            }

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
    public void setupCalibrationValues(float[] values)
    {
        mCalibrationValues = values.clone();
    }
    public float[] getCalibrationValues()
    {
        return mCalibrationValues;
    }

    public void enableCalibration(boolean value)
    {
        bCalibrationEnabled = value;
    }
}
