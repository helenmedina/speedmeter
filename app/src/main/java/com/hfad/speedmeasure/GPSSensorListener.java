package com.hfad.speedmeasure;

import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;


/**
 * Created by helen on 25/01/18.
 */


public class GPSSensorListener implements LocationListener{
    private GnssStatus.Callback mGnssStatusListener;
    private GPSController gpsController;
    private int numberSatellites = 0;
    private Metrics metrics;
    private Location lastLocation = null;

    public GPSSensorListener(GPSController gpsController)
    {
        this.gpsController = gpsController;
        metrics = new Metrics();
        mGnssStatusListener = null;
    }

    public GnssStatus.Callback addGnssStatusListener() {

        mGnssStatusListener = new GnssStatus.Callback() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onStopped() {
            }

            @Override
            public void onFirstFix(int ttffMillis) {
            }

            @Override
            public void onSatelliteStatusChanged(GnssStatus status) {
                int currentNumberSatellites = status.getSatelliteCount();
                if (currentNumberSatellites != numberSatellites)
                {
                    gpsController.ChangeSatelliteStatus(currentNumberSatellites);
                    numberSatellites = currentNumberSatellites;
                }

            }
        };
        return mGnssStatusListener;

    }

    @Override
    public void onLocationChanged(Location location) {
    if (location !=null)
    {
        float currentSpeed = 0;

        if(lastLocation == null)
        {
            lastLocation = location;

        }
        else
        {
            long distance = (long)Math.abs(getDistanceBetweenLocation(lastLocation,location));
            //long distance = (long)Math.abs((location.getLongitude() - lastLocation.getLongitude()));
            double time = (location.getTime() - lastLocation.getTime())/1000;
            currentSpeed = (float)(distance/time);

            metrics.setDistance(metrics.getDistance() + distance);
            metrics.setTime(metrics.getTime() + time);

            if(location.hasSpeed())
                metrics.setSpeed(location.getSpeed());
            else
                metrics.setSpeed(currentSpeed);

            lastLocation = location;
            gpsController.updateMetrics(metrics);
        }

    }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private double getDistanceBetweenLocation(Location one, Location two) {
        int R = 6371000;//meters
        Double dLat = toRad(two.getLatitude() - one.getLatitude());
        Double dLon = toRad(two.getLongitude() - one.getLongitude());
        Double lat1 = toRad(one.getLatitude());
        Double lat2 = toRad(two.getLatitude());
        Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (R * c);
    }

    private double toRad(Double d) {
        return d * Math.PI / 180;
    }
}
