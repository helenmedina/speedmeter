package com.hfad.speedmeasure;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by helen on 31/01/18.
 */

public abstract class DetectorBasedOnAccelerometer extends AppCompatActivity implements AccelerometerController.SensorListener{
    protected AccelerometerController accelerometerController = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void startListener() {

        accelerometerController.registerAccelerometer();

    }

    @Override
    public void stopListener() {

        accelerometerController.unregisterAccelerometer();

    }

    @Override
    public abstract void onAction();


}
