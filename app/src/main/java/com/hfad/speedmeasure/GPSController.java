package com.hfad.speedmeasure;

import android.app.Activity;
import android.content.Context;
import android.location.GnssStatus;
import android.location.LocationManager;
import android.location.Criteria;


import android.widget.Toast;
import android.content.pm.PackageManager;
import android.Manifest;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;

/**
 * Created by helen on 26/01/18.
 */

public class GPSController {

    private LocationManager locationManager;
    private GnssStatus.Callback gnsStatus;
    private GPSSensorListener gps;
    private boolean bSatelliteDetectorEnabled = false;
    private boolean bLocatorDetectorEnabled = false;
    private LocationDetectorListener locationListener;
    private static GPSController controller = null;


    private void startSatellitesDetection(String provider, Context context) {

        //ask for user permission
        ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        if(ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(context, "Permission not granted", Toast.LENGTH_SHORT).show();
        }

        gnsStatus = gps.addGnssStatusListener();
        locationManager.registerGnssStatusCallback(gnsStatus);
        locationManager.requestLocationUpdates(provider, 30000, 0, gps);
    }

    private void startGPSLocation(String provider, Context context) {

        //ask for user permission
        ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        if(ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(context, "Permission not granted", Toast.LENGTH_SHORT).show();
        }

        boolean isGPSEnabled = locationManager.isProviderEnabled(provider);
        if (isGPSEnabled) {

            locationManager.requestLocationUpdates(provider, 0, 0, gps);
        }
    }


    private GPSController(LocationDetectorListener listener, Context context)
    {
        locationListener = listener;
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        gps = new GPSSensorListener(this);
        gnsStatus = null;
    }

    public static void init (LocationDetectorListener listener, Context context)
    {

        if (controller == null)
            controller = new GPSController(listener, context);

    }

    public static GPSController getController()
    {
        return controller;
    }

    public void enableSatelliteDetector(boolean value)
    {
        bSatelliteDetectorEnabled = value;
    }
    public void enableGPSDetector(boolean value)
    {
        bLocatorDetectorEnabled = value;
    }

    public boolean isSatelliteDetectorEnabled()
    {
        return bSatelliteDetectorEnabled;
    }

    public boolean isGPSDetectorEnabled()
    {
        return bLocatorDetectorEnabled;
    }



    public static void removeController()
    {
        controller = null;
    }

    public void startProcess(Context context)
    {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String provider = locationManager.getBestProvider(criteria, true);


        if (bSatelliteDetectorEnabled)
            startSatellitesDetection(provider, context);
        if (bLocatorDetectorEnabled)
            startGPSLocation(provider, context);

    }


    public void removeSatellitesDetector()
    {

        locationManager.unregisterGnssStatusCallback(gnsStatus);

    }

    public void removeGPSDetector()
    {

        locationManager.removeUpdates(gps);
    }

    public void ChangeSatelliteStatus(int numberSatellite)
    {

        locationListener.onSatelliteAction(numberSatellite);
    }

    public void updateMetrics(Metrics metrics)
    {

        locationListener.onGPSAction(metrics);

    }

    public void startDetector()
    {
        locationListener.startListener();
    }

    public void stopDetector()
    {
        locationListener.stopListener();
    }

    //interface implemented in SpeedActivity
    public interface LocationDetectorListener
    {
        public void startListener();
        public void stopListener();
        public void onSatelliteAction(int numberSatellite);
        public void onGPSAction(Metrics metrics);
    }

}
