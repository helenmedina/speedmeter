package com.hfad.speedmeasure;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CalibrationActivity extends DetectorBasedOnAccelerometer implements ActivityCompat.OnRequestPermissionsResultCallback {

    Button startButton;
    Button calibrationButton;
    private float[] calibrationValues;
    private boolean bCalibrationEnabled;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);

        startButton = (Button) findViewById(R.id.start_button);
        startButton.setEnabled(false);
        calibrationButton = (Button) findViewById(R.id.calibration_button);
        setupAccelerometerController();

        bCalibrationEnabled = false;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        accelerometerController.removeController();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterAccelerometer();
        accelerometerController.removeController();

    }

    private void setupAccelerometerController()
    {
        AccelerometerController.init(this, getApplicationContext());
       accelerometerController = AccelerometerController.getController();
    }
    public void onClickStartCalibration(View view) {

        //Start calibration process
        bCalibrationEnabled = true;
        accelerometerController.startAccelerometer(bCalibrationEnabled);

    }

    public void onClickStart(View view) {

        bCalibrationEnabled = false;
        accelerometerController.startAccelerometer(bCalibrationEnabled);

       // startSpeedActivity();
    }

    public void unRegisterAccelerometer()
    {
        accelerometerController.unregisterAccelerometer();

    }
    public void finishCalibration()
    {
        calibrationValues = accelerometerController.getCalibrationValues();
        Toast.makeText(this, "Calibration finished", Toast.LENGTH_SHORT).show();
        enableStartButton(true);
    }
    public void enableStartButton(boolean bEnabled)
    {
        startButton.setEnabled(bEnabled);
    }


    public void startSpeedActivity()
    {

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck == PackageManager.PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }
        else
        {
            Intent intent = new Intent(this, StartSpeedActivity.class);
            intent.putExtra("calibration", calibrationValues);
            startActivity(intent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if ( ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED)
        {

            Toast.makeText(this, "Permission not granted " , Toast.LENGTH_SHORT).show();
        }
        else
        {
            startSpeedActivity();
        }

    }

    //Override Listener

    @Override
    public void stopListener() {
        super.stopListener();
        if(bCalibrationEnabled)
            finishCalibration();

    }

    @Override
    public void onAction() {
        Toast.makeText(this, "Movement detected", Toast.LENGTH_SHORT).show();
        startSpeedActivity();
    }

}
