package com.hfad.speedmeasure;

/**
 * Created by helen on 29/01/18.
 */

public  class StateSpeed {
    public static double SPEED_STOPPED_LOW = 0;
    public static double SPEED_STOPPED_HIGH = 1;
    public static double SPEED_WALKING_HIGH = 4;
    public static double SPEED_WALKING_FAST_HIGH = 6;
    public static double SPEED_RUNNING_HIGH = 12;
    public static double SPEED_SPRINT_HIGH = 25;
    public static double SPEED_MOTOR_HIGH = 170;

}
