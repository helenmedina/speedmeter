package com.hfad.speedmeasure;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by helen on 27/01/18.
 */

public class SpeedView extends View {

    private int height, width = 0;
    private int padding = 0;
    private int fontSize = 0;
    private int numeralSapcing = 0;
    private int radius = 0;
    private Paint paint;

    private boolean isInit;
    private int[] numbers = {0, 20, 40, 60, 80, 100, 120, 140, 160, 180};

    public double value=0;
    private Rect rect = new Rect();
    Canvas canvas = null;


    private void initSpeed()
    {
        height = getHeight();
        width = getWidth();
        padding = numeralSapcing + 50;
        fontSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 13, getResources().getDisplayMetrics());
        int min = Math.min(height, width);
        radius = min/2 - padding;
        paint = new Paint();
        isInit = true;
    }

    private void drawCircle(Canvas canvas){
        paint.reset();
        paint.setColor(getResources().getColor(android.R.color.white));
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        canvas.drawCircle(width/2, height/2, radius+ padding - 10, paint);

    }

    private void drawCenter(Canvas canvas){
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(width*2, height/2, 12, paint);

    }

    private void drawNumeral(Canvas canvas){
        paint.setTextSize(fontSize);
        int i = 0;
        for(int number : numbers)
        {

            String tmp = String.valueOf(number);
            paint.getTextBounds(tmp, 0, tmp.length(), rect);
            double angle = Math.PI/6*(i - 3) + Math.PI;
            float x = (float)(width/2 + Math.cos(angle)* radius - rect.width()/2);
            float y = (float)(height/2 + Math.sin(angle)* radius - rect.height()/2);
            canvas.drawText(tmp, x, y + y*0.05f, paint);
            i++;
        }

    }

    private void drawHands(Canvas canvas, double value){

        double angle = Math.PI/6*(value/20 - 3) + Math.PI;
        canvas.drawLine(width/2, height/2, (float)(width/2 + Math.cos(angle)*radius), (float)(height/2 + Math.sin(angle)*radius), paint);

    }

    public SpeedView(Context context) {
        super(context);
    }

    public SpeedView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SpeedView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        this.canvas = canvas;

        if(!isInit)
            initSpeed();

        canvas.drawColor(Color.BLACK);
        drawCircle(canvas);
        drawCenter(canvas);
        drawNumeral(canvas);
        drawHands(canvas, value);

        postInvalidateDelayed(500);
        invalidate();
    }

}
