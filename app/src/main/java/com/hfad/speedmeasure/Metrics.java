package com.hfad.speedmeasure;

/**
 * Created by helen on 27/01/18.
 */

public class Metrics{
    double speed = 0;
    long distance = 0;
    double time = 0;

    public Metrics(){
        speed = 0;
        distance = 0;
        time = 0;
    }
    public void setSpeed(double speed)
    {
        this.speed = speed;
    }
    public double getSpeed()
    {
        return speed;
    }
    public void setDistance(long distance)
    {
        this.distance = distance;
    }
    public long getDistance()
    {
        return distance;
    }
    public void setTime(double time)
    {
        this.time = time;
    }
    public double getTime()
    {
        return time;
    }
}