package com.hfad.speedmeasure;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import java.text.DecimalFormat;


public class StartSpeedActivity extends DetectorBasedOnGPS {

    //AccelerometerController accelerometerController;
    static private int MAX_SPEED_ALLOWED = R.integer.maxSeed;
    UserState userState = null;


    @Override
    protected void onStop() {
        super.onStop();
        stopTracking();
        GPSController.removeController();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_speed);
        Resources res = getResources();
        int initialState = res.getInteger(R.integer.initialState);

        userState = new UserState(UserState.getState(initialState), this);

        setupInitialGPS();
        startSpeedMeasure();

    }

    @Override
    public void onSatelliteAction(int numberSatellite) {
        updateStatusSatellite(numberSatellite);
    }

    @Override
    public void onGPSAction(Metrics metrics) {
        updateMetrics(metrics);
    }

    @Override
    public void startListener()
    {
        gpsController.startProcess(this);
    }

    private void stopTracking()
    {
        gpsController.stopDetector();
    }

    private void updateDistance(Metrics metrics)
    {
        String newDistance = Long.toString(metrics.getDistance());
        TextView distanceText = (TextView)this.findViewById(R.id.distanceValue);
        String currentDistance = distanceText.getText().toString();
        if (newDistance != currentDistance)
        {

            newDistance = newDistance + " m";
            distanceText.setText(newDistance);
        }

    }

    private void updateSpeed(Metrics metrics)
    {

        DecimalFormat df2 = new DecimalFormat(".##");
        double speedKmMetrics = metrics.getSpeed()*3.6;
        String newSpeed = df2.format(speedKmMetrics);

        TextView speedText = (TextView)this.findViewById(R.id.speedValue);
        String currentSpeed = speedText.getText().toString();
        if (newSpeed != currentSpeed)
        {
            newSpeed = newSpeed + " km/h";
            speedText.setText(newSpeed);
        }

        updateState(speedKmMetrics);
        updateSpeedUI(speedKmMetrics);

    }

    private void updateTime(Metrics metrics)
    {
        String newTime = Double.toString(metrics.getTime());
        TextView timeText = (TextView)this.findViewById(R.id.timeValue);
        String currentTime = timeText.getText().toString();
        if (newTime != currentTime)
        {
            newTime = newTime + " sec";
            timeText.setText(newTime);
        }

    }

    private void updateSpeedUI(double speed)
    {
        SpeedView speedView = (SpeedView)this.findViewById(R.id.speedView);
        if (speed > MAX_SPEED_ALLOWED)
            speedView.value = MAX_SPEED_ALLOWED;
        else
            speedView.value = speed;

        //reset speedView UI
        speedView.invalidate();
    }

    private void updateState(double speed)
    {
        TextView stateView = (TextView)this.findViewById(R.id.state);
        StateTypes newState = UserState.getStateFromSpeed(speed);

        //if state is next or previous state of current state
        if (userState.belongsToDeadBand(newState))
        {
            if( userState.isTimerFinished())
            {
                //if timer finished is configured same state
                if (userState.isStateValidForTimerConfiguration(newState))
                {
                    userState.setNewState(newState);
                    stateView.setText(userState.getStringFromStateType(newState));
                    userState.resetTimer();
                }
                else
                {
                    userState.resetTimer();
                    updateState(speed);
                }

            }
            else
            {
                if(!userState.bIsTimerRunning(newState))
                {
                    userState.startTimer(newState);
                }
            }

        }
        else
        {
            userState.setNewState(newState);
            stateView.setText(userState.getStringFromStateType(newState));
            userState.resetTimer();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_actions, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.calibrate_menu:
                Intent intent = new Intent(this, CalibrationActivity.class);
                startActivity(intent);
                return true;
            case R.id.stop_menu:
                stopTracking();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setupInitialGPS()
    {
        GPSController.init(this, getApplicationContext());
        gpsController = GPSController.getController();
    }
    public void startSpeedMeasure()
    {
        gpsController.enableGPSDetector(true);
        gpsController.enableSatelliteDetector(true);
        gpsController.startDetector();
    }

    public void updateStatusSatellite(int numberSatellite)
    {
        TextView satelliteText = (TextView)this.findViewById(R.id.numSatellite);
        satelliteText.setText(Integer.toString(numberSatellite));
    }


    public void updateMetrics(Metrics metrics)
    {

        updateDistance(metrics);
        updateSpeed(metrics);
        updateTime(metrics);


    }


}
