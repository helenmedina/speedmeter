package com.hfad.speedmeasure;

/**
 * Created by helen on 28/01/18.
 */

import android.content.Context;
import android.os.CountDownTimer;


public class UserState {

    private DeadBandOfState currentStates = null;
    private boolean bTimerNextStateEnabled= false;
    private boolean bTimerPreviousStateEnabled= false;
    private boolean bTimerFinished= false;
    private int time = 0; //miliseconds
    private Context context;
    private CountDownTimer cTimer = null;
    private StateTypes stateForTimerConfiguration;

    private static class DeadBandOfState
    {
        public  StateTypes current;
        private  StateTypes next;
        private StateTypes previous;

       private DeadBandOfState(StateTypes current, StateTypes next, StateTypes previous) {
           this.current = current;
           this.next = next;
           this.previous = previous;
       }
    }

    private DeadBandOfState getDeadBand(StateTypes state)
    {
        int length = statesArray.length;
        int i = 0;
        for (; i < length; i++)
        {
            if (statesArray[i].current == state)
            {
                break;
            }
        }

        return statesArray[i];

    }

    public static  DeadBandOfState[] statesArray = {
            new DeadBandOfState(StateTypes.STOPPED, StateTypes.WALKING, null),
            new DeadBandOfState(StateTypes.WALKING, StateTypes.WALKING_FAST, StateTypes.STOPPED),
            new DeadBandOfState(StateTypes.WALKING_FAST, StateTypes.RUNNING, StateTypes.WALKING),
            new DeadBandOfState(StateTypes.RUNNING, StateTypes.SPRINT, StateTypes.WALKING_FAST),
            new DeadBandOfState(StateTypes.SPRINT, StateTypes.MOTOR_SPEED, StateTypes.RUNNING),
            new DeadBandOfState(StateTypes.MOTOR_SPEED, StateTypes.AERIAL_SPEED, StateTypes.SPRINT),
            new DeadBandOfState(StateTypes.AERIAL_SPEED, null, StateTypes.MOTOR_SPEED)
    };

    public static StateTypes getState(int pos)
    {
        return statesArray[pos].current;
    }


    public UserState(StateTypes currentState, Context context)
    {

        currentStates = getDeadBand(currentState);
        this.context = context;

    }
    public boolean bIsTimerRunning(StateTypes state)
    {
        if (state == this.currentStates.next)
            return bTimerNextStateEnabled;
        else if (state == this.currentStates.previous)
            return bTimerPreviousStateEnabled;
        else
            return false;
    }

    public void startTimer(StateTypes state) {

        setTimer(state);

        if (state == this.currentStates.next) {
            if (bIsTimerRunning(this.currentStates.previous))
                cancelTimer();
            bTimerNextStateEnabled = true;
        }
        else if (state == this.currentStates.previous) {
            if (bIsTimerRunning(this.currentStates.next))
                cancelTimer();
            bTimerPreviousStateEnabled = true;
        }

        cTimer = new CountDownTimer(time, 100) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                bTimerFinished = true;
                cancelTimer();
            }
        };
        cTimer.start();


    }

    public void resetTimer() {
        bTimerFinished = false;
        bTimerPreviousStateEnabled = false;
        bTimerNextStateEnabled = false;
    }

    public boolean isTimerFinished() {
        return bTimerFinished;
    }

    public void cancelTimer() {

        bTimerNextStateEnabled = false;
        bTimerPreviousStateEnabled = false;
        if(cTimer!=null)
            cTimer.cancel();
    }

    public  void setTimer(StateTypes stateFoundInDeadBand)
    {
        stateForTimerConfiguration = stateFoundInDeadBand;

        if (currentStates.current == StateTypes.STOPPED) {
            if (stateFoundInDeadBand == StateTypes.WALKING)
                time = context.getResources().getInteger(R.integer.STOPPED_TO_WALKING);
        }

        else if (currentStates.current == StateTypes.WALKING) {
            if (stateFoundInDeadBand == StateTypes.WALKING_FAST)
                time = context.getResources().getInteger(R.integer.WALKING_TO_WALKING_FAST);
            else if (stateFoundInDeadBand == StateTypes.STOPPED)
                time = context.getResources().getInteger(R.integer.WALKING_TO_STOPPED);
        }

        else if (currentStates.current == StateTypes.WALKING_FAST) {
            if (stateFoundInDeadBand == StateTypes.RUNNING)
                time = context.getResources().getInteger(R.integer.WALKING_FAST_TO_RUNNING);
            else if (stateFoundInDeadBand == StateTypes.WALKING)
                time = context.getResources().getInteger(R.integer.WALKING_FAST_TO_WALKING);
        }

        else if (currentStates.current == StateTypes.RUNNING) {
            if (stateFoundInDeadBand == StateTypes.SPRINT)
                time = context.getResources().getInteger(R.integer.RUNNING_TO_SPRINT);
            else if (stateFoundInDeadBand == StateTypes.WALKING_FAST)
                time = context.getResources().getInteger(R.integer.RUNNING_TO_WALKING_FAST);
        }
        else if (currentStates.current == StateTypes.SPRINT) {
            if (stateFoundInDeadBand == StateTypes.MOTOR_SPEED)
                time = context.getResources().getInteger(R.integer.SPRINT_TO_MOTOR_SPEED);
            else if (stateFoundInDeadBand == StateTypes.RUNNING)
                time = context.getResources().getInteger(R.integer.SPRINT_TO_RUNNING);
        }

        else if (currentStates.current == StateTypes.MOTOR_SPEED) {
            if (stateFoundInDeadBand == StateTypes.AERIAL_SPEED)
                time = context.getResources().getInteger(R.integer.MOTOR_SPEED_TO_AERIAL_MOTOR);
            else if (stateFoundInDeadBand == StateTypes.SPRINT)
                time = context.getResources().getInteger(R.integer.MOTOR_SPEED_TO_SPRINT);
        }
        else if (currentStates.current == StateTypes.AERIAL_SPEED) {
            if (stateFoundInDeadBand == StateTypes.MOTOR_SPEED)
                time = context.getResources().getInteger(R.integer.AERIAL_MOTOR_TO_MOTOR_SPEED);

        }

    }

    public boolean belongsToDeadBand(StateTypes state)
    {
        if(this.currentStates.next == state || this.currentStates.previous == state)
        {
            return true;
        }
        else
            return false;
    }

    public void setNewState(StateTypes newState)
    {
        currentStates = getDeadBand(newState);
    }

    public static StateTypes getStateFromSpeed(double speed)
    {
        StateTypes state = StateTypes.STOPPED;
        if ( StateSpeed.SPEED_STOPPED_LOW <=  speed && speed < StateSpeed.SPEED_STOPPED_HIGH)
            state = StateTypes.STOPPED;
        else if ( StateSpeed.SPEED_STOPPED_HIGH <=  speed && speed < StateSpeed.SPEED_WALKING_HIGH)
            state = StateTypes.WALKING;
        else if ( StateSpeed.SPEED_WALKING_HIGH <=  speed && speed < StateSpeed.SPEED_WALKING_FAST_HIGH)
            state = StateTypes.WALKING_FAST;
        else if ( StateSpeed.SPEED_WALKING_FAST_HIGH <=  speed && speed < StateSpeed.SPEED_RUNNING_HIGH)
            state = StateTypes.RUNNING;
        else if ( StateSpeed.SPEED_RUNNING_HIGH <=  speed && speed < StateSpeed.SPEED_SPRINT_HIGH)
            state = StateTypes.SPRINT;
        else if ( StateSpeed.SPEED_SPRINT_HIGH <=  speed && speed < StateSpeed.SPEED_MOTOR_HIGH)
            state = StateTypes.MOTOR_SPEED;
        else if ( StateSpeed.SPEED_MOTOR_HIGH <=  speed )
            state = StateTypes.AERIAL_SPEED;


        return state;
    }

    public String getStringFromStateType(StateTypes state)
    {
        switch(state)
        {
            case STOPPED: return "Stopped";
            case WALKING: return "Walking";
            case WALKING_FAST: return "Walking fast";
            case RUNNING: return "Running";
            case SPRINT: return "Sprint";
            case MOTOR_SPEED: return "Vehicle Motor";
            case AERIAL_SPEED: return "Aerial Motor";
            default: return  "Stopped";

        }
    }

    public boolean isStateValidForTimerConfiguration(StateTypes state)
    {
       return (stateForTimerConfiguration == state);
    }

}
